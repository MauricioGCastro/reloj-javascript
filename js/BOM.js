// function saludar() {
//     alert("Hola testoy saludando!");

// }

//window.setTimeout(saludar, 4000); 
//Este metodo ejecuta una funcion despues del tiempo indicado en milisegundos

// let tiempo = window.setInterval(contar, 1000);

// let contador = 1;

// function contar() {
//     document.write(contador + "<br>");
//     contador++;
// if(contador ==10){
//     window.clearInterval(tiempo);
// }
// }

function obtenerHora() {
    let fecha = new Date();

    //Traer las etiquetas HTML
    let pDiaSemana = document.getElementById(`diasemana`), //siseparo con comas puedo declarar muchas variables
        pDia = document.getElementById(`dia`),
        pMes = document.getElementById(`mes`),
        pAnio = document.getElementById(`anio`),
        pHoras = document.getElementById(`horas`),
        pMinutos = document.getElementById(`minutos`),
        pSegundos = document.getElementById(`segundos`),
        pMeridianos = document.getElementById(`meridiano`);
    let diaSemanas = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"];
    let mesMes = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "octubre", "Noviembre", "Diciembre"];

    //Asignar valores (de los dias de la semana)
    pDiaSemana.innerText = diaSemanas[fecha.getDay()];
    pDia.innerText = fecha.getDate();
    pMes.innerText = mesMes[fecha.getMonth()];
    pAnio.innerText = fecha.getFullYear();

    // pHoras.innerText = fecha.getHours();
    // pMinutos.innerText = fecha.getMinutes();
    // pSegundos.innerText = fecha.getSeconds();

    if (fecha.getHours() > 12) {
        if ((pHoras.innerText = fecha.getHours() - 12) < 10) {
            pHoras.innerText = "0" + (fecha.getHours() - 12);
        } else {
            pHoras.innerText = fecha.getHours() - 12;
        }
    }

    if (fecha.getHours() >= 12) {
            pMeridianos.innerText= "PM";
        }else{
            pMeridianos.innerText= "AM";
        }

        if (fecha.getMinutes() < 10) {
            pMinutos.innerText = "0" + fecha.getMinutes();
        } else {
            pMinutos.innerText = fecha.getMinutes();
        }

        if (fecha.getSeconds() < 10) {
            pSegundos.innerText = "0" + fecha.getSeconds();
        } else {
            pSegundos.innerText = fecha.getSeconds();
        }
    }
    window.setInterval(obtenerHora, 1000);
